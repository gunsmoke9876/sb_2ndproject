// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Sb_2ndProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SB_2NDPROJECT_API ASb_2ndProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
